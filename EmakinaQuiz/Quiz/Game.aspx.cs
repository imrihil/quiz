﻿using EmakinaQuiz.Logic;
using EmakinaQuiz.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EmakinaQuiz.Quiz
{
    public partial class Game : AbstractGamePage
    {
        protected override void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender, e);
            if (!IsPostBack)
            {
                if (!IsGameActive())
                    return;
                InitControls();
            }
        }

        private bool IsGameActive()
        {
            if (GetGame().IsWon)
            {
                Response.Redirect($"~/Quiz/Win.aspx?gameId={GameId}");
                return false;
            }
            else if (GetGame().IsLost)
            {
                Response.Redirect($"~/Quiz/Defeat.aspx?gameId={GameId}");
                return false;
            }
            return true;
        }

        private void InitControls()
        {
            GameQuestion nextGameQuestion = GetGame().NextQuestion;
            GameQuestions.GameQuestionId = nextGameQuestion.GameQuestionId;

            GameProgressBarDone.Attributes["aria-valuenow"] = (nextGameQuestion.Number - 1).ToString();
            GameProgressBarDone.Style.Add("width", (100 * (nextGameQuestion.Number - 1) / 5) + "%");
            GameProgressBar.InnerText = nextGameQuestion.Number + "/5";
        }

        public void LeaveGameButton_Click(object sender, EventArgs e)
        {
            GameActions actions = new GameActions();
            Models.Game game = actions.EndGame(GameId);
            Response.Redirect("~/Default.aspx");
        }
    }
}
