﻿using EmakinaQuiz.Logic;
using EmakinaQuiz.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EmakinaQuiz.Quiz
{
    public partial class Win : AbstractGamePage
    {
        protected override void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender, e);
            if (!IsPostBack)
            {
                InitControls();
            }
        }

        private void InitControls()
        {
            GameTimeValue.Text = GetGame().TimeString;
            GamesList.CurrentGameId = GameId;
        }
    }
}
