﻿<%@ Page Title="Defeat" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Defeat.aspx.cs" Inherits="EmakinaQuiz.Quiz.Defeat" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>

    <h4><asp:Label ID="MessageLabel" Text="Do not give up. Try again!" runat="server"></asp:Label></h4>

    <asp:Button ID="StartGameButton" runat="server" Text="Start new game" OnClick="StartGameButton_Click" CssClass="btn btn-primary btn-lg btn-block" />
</asp:Content>
