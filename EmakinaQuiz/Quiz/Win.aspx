﻿<%@ Page Title="Win" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Win.aspx.cs" Inherits="EmakinaQuiz.Quiz.Win" %>

<%@ Register TagPrefix="uc" TagName="GamesList" Src="~/Controls/GamesListControl.ascx" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>

    <h4>
        <asp:Label ID="GameTimeLabel" Text="Your time: " runat="server"></asp:Label><asp:Label ID="GameTimeValue" runat="server"></asp:Label>
    </h4>

    <uc:GamesList ID="GamesList" runat="server"></uc:GamesList>

    <asp:Button ID="StartGameButton" runat="server" Text="Start new game" OnClick="StartGameButton_Click" CssClass="btn btn-primary btn-lg btn-block" />
</asp:Content>
