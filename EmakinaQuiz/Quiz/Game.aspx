﻿<%@ Page Title="Game" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Game.aspx.cs" Inherits="EmakinaQuiz.Quiz.Game" %>

<%@ Register TagPrefix="uc" TagName="GameQuestion" Src="~/Controls/GameQuestionControl.ascx" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-10">
            <h2><%: Title %>.</h2>
        </div>

        <div class="col-md-2">
            <asp:FormView ID="GameForm" ItemType="EmakinaQuiz.Models.Game" SelectMethod="GetGame" RenderOuterTable="false" runat="server">
                <ItemTemplate>
                    <asp:HiddenField ID="GameStartDate" Value="<%#:Item.StartTime.Value.ToUniversalTime()%>" runat="server" />
                    <h4 class="float-right"><asp:Label ID="GameTime" Text="<%#:Item.TimeString%>" runat="server" /></h4>
                </ItemTemplate>
            </asp:FormView>
        </div>
    </div>

    <div id="GameProgress" class="progress">
        <div id="GameProgressBarDone" class="progress-bar progress-bar-striped" role="progressbar" aria-valuemin="0" aria-valuemax="5" runat="server"></div>
        <div id="GameProgressBar" class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: 20%" aria-valuenow="1" aria-valuemin="0" aria-valuemax="5" runat="server"></div>
    </div>

    <uc:GameQuestion ID="GameQuestions" runat="server"></uc:GameQuestion>

    <asp:Button ID="LeaveGameButton" CssClass="btn btn-lg btn-block" OnClick="LeaveGameButton_Click" Text="Leave game" runat="server" />

    <script>
        var st = $("[id$=GameStartDate]").val();
        var pattern = /(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2}):(\d{2})/;
        var start = new Date(st.replace(pattern, '$3-$2-$1T$4:$5:$6Z'));

        setInterval(function () {
            $("[id$=GameTime]").text(dateDifference(start, new Date()));
        }, 20);
    </script>
</asp:Content>
