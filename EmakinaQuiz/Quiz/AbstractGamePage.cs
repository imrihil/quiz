﻿using EmakinaQuiz.Exceptions;
using EmakinaQuiz.Logic;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;

namespace EmakinaQuiz.Quiz
{
    public abstract class AbstractGamePage : Page
    {
        public long GameId
        {
            get
            {
                long? gId = (long?)ViewState["GameId"];
                return ((gId == null) ? 0 : gId.Value);
            }
            set
            {
                ViewState["GameId"] = value;
            }
        }

        private Models.Game _game { get; set; }

        protected virtual void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HandleQueryStrings();
            }
            Authorize();
        }

        private void HandleQueryStrings()
        {
            long gameId;
            if (Request.QueryString["gameId"] == null)
                throw new ArgumentNullException("ERROR: It is illegal to load Win.aspx without setting a gameId.");
            if (!long.TryParse(Request.QueryString["gameId"], out gameId))
                throw new ArgumentException("ERROR: The gameId must be number.");
            GameId = gameId;
        }

        private void Authorize()
        {
            string userId = User.Identity.GetUserId();
            if (userId != null)
            {
                if (GetGame().ApplicationUserId != userId)
                {
                    throw new ForbiddenAccessException("AbstractGamePage forbidden access.") { Source = Request.RawUrl };
                }
            }
            else
            {
                throw new UnauthorizedAccessException("AbstractGamePage unauthorized access.") { Source = Request.RawUrl };
            }
        }

        public void StartGameButton_Click(object sender, EventArgs e)
        {
            string userId = User.Identity.GetUserId();
            if (userId != null)
            {
                GameActions actions = new GameActions();
                Models.Game game = actions.GetActiveGameByUserId(userId);
                if (game == null)
                    game = actions.CreateGame(userId);
                Response.Redirect("~/Quiz/Game.aspx?gameId=" + game.GameId);
            }
            else
            {
                throw new UnauthorizedAccessException("StartGameButton_Click unauthorized access.") { Source = Request.RawUrl };
            }
        }

        public Models.Game GetGame()
        {
            if (_game == null)
            {
                GameActions actions = new GameActions();
                _game = actions.GetGameById(GameId);
            }
            return _game;
        }
    }
}
