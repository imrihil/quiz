﻿function dateDifference(dateFrom, dateTo) {
    var difms = dateTo - dateFrom;
    var ms = difms % 1000;
    var s = Math.floor(difms / 1000) % 60;
    var m = Math.floor(difms / 60000) % 60;
    var h = Math.floor(difms / 3600000);
    return h + ":" + m.pad(2) + ":" + s.pad(2) + "." + ms.pad(3)
}
