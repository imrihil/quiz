﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="EmakinaQuiz.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <address>
        <p><strong>Author:</strong> Mateusz Ledzianowski</p>
        <p><strong>Mail:</strong> <a href="mailto:ledzianowskimateusz@gmail.com">ledzianowskimateusz@gmail.com</a></p>
    </address>
</asp:Content>
