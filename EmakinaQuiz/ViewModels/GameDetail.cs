﻿using EmakinaQuiz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmakinaQuiz.ViewModels
{
    public class GameDetail
    {
        public long GameId { get; set; }

        public int Place { get; set; }

        public string UserName { get; set; }

        public DateTime? EndTime { get; set; }

        public string TimeString { get; set; }

        public GameDetail(int place, Game game)
        {
            Place = place;
            GameId = game.GameId;
            UserName = game.ApplicationUser.UserName;
            EndTime = game.EndTime;
            TimeString = game.TimeString;
        }
    }
}