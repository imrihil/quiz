﻿using EmakinaQuiz.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EmakinaQuiz.Logic
{
    public class GameActions : AbstractQuizActions
    {
        public Game CreateGame(string userId)
        {
            Game game = new Game()
            {
                ApplicationUserId = userId
            };

            Db.Games.Add(game);
            Db.SaveChanges();

            Random random = new Random();
            IEnumerable<Question> questions = Db.Questions.ToList().OrderBy(x => random.Next()).Take(5);

            int i = 1;
            foreach (Question question in questions)
            {
                GameQuestion gameQuestion = new GameQuestion()
                {
                    GameId = game.GameId,
                    QuestionId = question.QuestionId,
                    Number = i++
                };
                Db.GameQuestions.Add(gameQuestion);
                Db.SaveChanges();
            }

            game.StartTime = DateTime.Now;
            Db.Entry(game).State = EntityState.Modified;
            Db.SaveChanges();

            return game;
        }

        public Game EndGame(long gameId)
        {
            Game game = Db.Games.Find(gameId);
            game.EndTime = DateTime.Now;
            Db.Entry(game).State = EntityState.Modified;
            Db.SaveChanges();
            return game;
        }

        public ICollection<Game> GetGames()
        {
            ICollection<Game> games = Db.Games.ToList();
            return games;
        }

        public Game GetActiveGameByUserId(string userId)
        {
            Game game = Db.Games.FirstOrDefault(x => x.ApplicationUserId == userId && x.EndTime == null);
            return game;
        }

        public Game GetGameById(long gameId)
        {
            Game game = Db.Games.FirstOrDefault(g => g.GameId == gameId);
            return game;
        }
    }
}
