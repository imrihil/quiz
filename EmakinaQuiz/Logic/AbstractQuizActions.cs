﻿using EmakinaQuiz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmakinaQuiz.Logic
{
    public abstract class AbstractQuizActions : IDisposable
    {
        protected QuizContext Db = new QuizContext();

        public void Dispose()
        {
            if (Db != null)
            {
                Db.Dispose();
                Db = null;
            }
        }
    }
}