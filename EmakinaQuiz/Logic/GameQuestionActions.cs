﻿using EmakinaQuiz.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EmakinaQuiz.Logic
{
    public class GameQuestionActions : AbstractQuizActions
    {
        public GameQuestion GetGameQuestionById(long gameQuestionId)
        {
            GameQuestion gameQuestion = Db.GameQuestions.FirstOrDefault(gq => gq.GameQuestionId == gameQuestionId);
            return gameQuestion;
        }

        public GameQuestion AnswerQuestion(long gameQuestionId, long questionAnswerId)
        {
            GameQuestion gameQuestion = Db.GameQuestions.FirstOrDefault(x => x.GameQuestionId == gameQuestionId);
            QuestionAnswer questionAnswer = gameQuestion.Question.QuestionAnswer.FirstOrDefault(x => x.QuestionAnswerId == questionAnswerId);
            if (questionAnswer == null)
                throw new ArgumentException("There is no question answer with given gameQuestionId and questionAnswerId.");
            gameQuestion.QuestionAnswer = questionAnswer;

            if (!gameQuestion.IsCorrect)
                gameQuestion.Game.EndTime = DateTime.Now;
            else if (gameQuestion.Game.GameQuestion.All(x => x.IsCorrect))
                gameQuestion.Game.EndTime = DateTime.Now;

            Db.Entry(gameQuestion).State = EntityState.Modified;
            Db.SaveChanges();
            return gameQuestion;
        }
    }
}