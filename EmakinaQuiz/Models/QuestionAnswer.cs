﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EmakinaQuiz.Models
{
    public class QuestionAnswer
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), ScaffoldColumn(false)]
        public long QuestionAnswerId { get; set; }

        [Required]
        public long AnswerId { get; set; }

        [Required]
        public long QuestionId { get; set; }

        public virtual Answer Answer { get; set; }

        public virtual Question Question { get; set; }
    }
}