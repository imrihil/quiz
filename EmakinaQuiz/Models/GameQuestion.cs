﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EmakinaQuiz.Models
{
    public class GameQuestion
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), ScaffoldColumn(false)]
        public long GameQuestionId { get; set; }

        [Required]
        public long Number { get; set; }

        [Required]
        public long GameId { get; set; }

        [Required]
        public long QuestionId { get; set; }
        
        public long? QuestionAnswerId { get; set; }

        public virtual Game Game { get; set; }

        public virtual Question Question { get; set; }

        public virtual QuestionAnswer QuestionAnswer { get; set; }

        public bool IsCorrect
        {
            get
            {
                if (QuestionAnswer == null)
                    return false;
                return QuestionAnswer.AnswerId == Question.AnswerId;
            }
        }
    }
}