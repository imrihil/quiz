﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EmakinaQuiz.Models
{
    public class QuizDatabaseInitializer : DropCreateDatabaseIfModelChanges<QuizContext>
    {
        private static readonly string[] _yesQuestions = {
            "Is a tortoise fast?",
            "Can a parrot speak?",
            "Is a goose white?",
            "Is the bat the only mammal that can fly?",
            "Can a tarantula live two years without any food?",
            "Can a cow sleep standing up?",
            "Can a hippo run faster than a human?",
            "Is a blue whale’s tooth heavier than an elephant?",
            "Is a goat the first animal domesticated by a man?",
            "Is a cooked onion harmful for dogs?"
        };

        private static readonly string[] _noQuestions = {
            "Is a duck a kind of fish?",
            "Does a horse have two horns?",
            "Can a penguin fly?",
            "Can a hen song?",
            "Can an elephant jump?",
            "Does a deer have a gall bladder?",
            "Can an ant sleep?",
            "Can a baby horse run just after a birth?",
            "Can a skunk bite and throw its scent at the same time?",
            "Is a dog as smart as a four-year-old child?"
        };

        protected override void Seed(QuizContext context)
        {
            GetAnswers().ForEach(x => context.Answers.Add(x));
            GetQuestions().ForEach(x => context.Questions.Add(x));
            GetQuestionAnswers().ForEach(x => context.QuestionAnswers.Add(x));
            context.SaveChanges();
        }

        private static List<Answer> GetAnswers()
        {
            var answers = new List<Answer> {
                new Answer
                {
                    AnswerId = 1,
                    Text = "Yes"
                },
                new Answer
                {
                    AnswerId = 2,
                    Text = "No"
                }
            };

            return answers;
        }

        private static List<Question> GetQuestions()
        {
            List<Question> questions = new List<Question>();
            for (int i = 0; i < _yesQuestions.Length; ++i)
            {
                questions.Add(new Question
                {
                    QuestionId = i + 1,
                    Text = _yesQuestions[i],
                    AnswerId = 1
                });
            }
            for (int i = 0; i < _noQuestions.Length; ++i)
            {
                questions.Add(new Question
                {
                    QuestionId = _yesQuestions.Length + i + 1,
                    Text = _noQuestions[i],
                    AnswerId = 2
                });
            }

            return questions;
        }

        private static List<QuestionAnswer> GetQuestionAnswers()
        {
            List<QuestionAnswer> questionAnswers = Enumerable.Range(0, 2 * (_yesQuestions.Length + _noQuestions.Length))
                .Select(x => new QuestionAnswer
                {
                    QuestionAnswerId = x + 1,
                    AnswerId = x % 2 + 1,
                    QuestionId = x / 2 + 1
                }).ToList();

            return questionAnswers;
        }
    }
}