﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EmakinaQuiz.Models
{
    public class Game
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), ScaffoldColumn(false)]
        public long GameId { get; set; }

        [Required]
        public string ApplicationUserId { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual ICollection<GameQuestion> GameQuestion { get; set; }

        public TimeSpan? Time
        {
            get
            {
                if (StartTime.HasValue && EndTime.HasValue)
                    return EndTime - StartTime;
                if (StartTime.HasValue)
                    return DateTime.Now - StartTime;
                return null;
            }
        }

        public GameQuestion NextQuestion
        {
            get
            {
                return GameQuestion.Where(x => x.QuestionAnswerId == null).OrderBy(x => x.Number).FirstOrDefault();
            }
        }

        public bool IsWon
        {
            get
            {
                return EndTime != null && GameQuestion.All(x => x.IsCorrect);
            }
        }

        public bool IsLost
        {
            get
            {
                return EndTime != null && GameQuestion.Any(x => !x.IsCorrect);
            }
        }

        public string TimeString
        {
            get
            {
                if (Time.HasValue)
                    return string.Format("{0}:{1:00}:{2:00}:{3:000}", ((int)Time.Value.TotalHours), Time.Value.Minutes, Time.Value.Seconds, Time.Value.Milliseconds);
                else
                    return "";
            }
        }
    }
}