﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EmakinaQuiz.Models
{
    public class Question
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), ScaffoldColumn(false)]
        public long QuestionId { get; set; }

        [Required, StringLength(1000)]
        public string Text { get; set; }

        [Required]
        public long? AnswerId { get; set; }
        
        public virtual Answer Answer { get; set; }

        public virtual ICollection<QuestionAnswer> QuestionAnswer { get; set; }

        public virtual ICollection<GameQuestion> GameQuestion { get; set; }
    }
}