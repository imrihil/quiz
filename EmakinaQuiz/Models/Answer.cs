﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EmakinaQuiz.Models
{
    public class Answer
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), ScaffoldColumn(false)]
        public long AnswerId { get; set; }

        [Required, StringLength(1000)]
        public string Text { get; set; }

        public virtual ICollection<QuestionAnswer> QuestionAnswer { get; set; }

        public virtual ICollection<Question> Question { get; set; }
    }
}