﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="EmakinaQuiz.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <ul>
        <li>Every game has 5 yes-no questions.</li>
        <li>You win if you answer to all the questions.</li>
        <li>The time from the first question to the answer to the last one is the result of the game.</li>
    </ul>
</asp:Content>
