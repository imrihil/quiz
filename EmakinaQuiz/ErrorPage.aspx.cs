﻿using EmakinaQuiz.Exceptions;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EmakinaQuiz
{
    public partial class ErrorPage : Page
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly string BadRequestText = "An HTTP error occurred. Bad Request (400). The server cannot or will not process the request due to something that is perceived to be a client error (e.g., malformed request syntax, invalid request message framing, or deceptive request routing). Please try again.";
        private static readonly string Unauthorized = "An HTTP error occurred. Unauthorized access (401). The request has not been applied because it lacks valid authentication credentials for the target resource. Please log in.";
        private static readonly string Forbidden = "An HTTP error occurred. Access forbidden (403). The user might not have the necessary permissions for a resource, or may need an account of some sort.";
        private static readonly string NotFound = "An HTTP error occurred. Page not found (404). The origin server did not find a current representation for the target resource or is not willing to disclose that one exists. Please try again.";
        private static readonly string Timeout = "An HTTP error occurred. Request Timeout (408). The server did not receive a complete request message within the time that it was prepared to wait. Please try again later.";
        private static readonly string Conflict = "An HTTP error occurred. Conflict (409). The request could not be processed because of conflict in the current state of the resource. Refresh the page and try again.";
        private static readonly string InternalServerError = "An server error occured. Internal Server Error (500). The server encountered an unexpected condition that prevented it from fulfilling the request.";
        private static readonly string LoopDetected = "An server error occured. Loop Detected (508). The server encountered an unexpected condition that prevented it from fulfilling the request.";
        private static readonly string GeneralError = "An HTTP error occurred. Please try again. If this error continues, please contact support.";

        protected void Page_Load(object sender, EventArgs e)
        {
            string errorCode = Request.QueryString["code"];
            if (errorCode != null)
            {
                switch (errorCode)
                {
                    case "400": FriendlyErrorMessage.Text = BadRequestText; break;
                    case "401": FriendlyErrorMessage.Text = Unauthorized; Response.Redirect("~/Account/Login.aspx"); break;
                    case "403": FriendlyErrorMessage.Text = Forbidden; break;
                    case "404": FriendlyErrorMessage.Text = NotFound; break;
                    case "408": FriendlyErrorMessage.Text = Timeout; break;
                    case "409": FriendlyErrorMessage.Text = Conflict; break;
                    case "500": FriendlyErrorMessage.Text = InternalServerError; break;
                    case "508": FriendlyErrorMessage.Text = LoopDetected; break;
                    default: FriendlyErrorMessage.Text = GeneralError; break;
                }
            }
            else
            {
                FriendlyErrorMessage.Text = GeneralError;
            }

            Exception ex = Server.GetLastError();
            if (ex == null)
            {
                ex = new Exception("The error was unhandled by application code.");
            }
            else if (ex is HttpUnhandledException)
            {
                if (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                }
            }

            if (ex is UnauthorizedAccessException)
            {
                Response.Redirect("~/Account/Login.aspx");
            }
            else if (ex is ForbiddenAccessException)
            {
                FriendlyErrorMessage.Text = Forbidden;
            }

            if (Request.IsLocal)
            {
                DetailedErrorPanel.Visible = true;

                ErrorDetailedMessage.Text = ex.Message;

                string errorHandler = Request.QueryString["handler"];
                if (errorHandler != null)
                {
                    ErrorHandler.Text = errorHandler;
                }

                if (ex.InnerException != null)
                {
                    InnerMessage.Text = ex.GetType().ToString() + "<br />" +
                        ex.InnerException.Message;
                    InnerTrace.Text = ex.InnerException.StackTrace.Replace("\n", "<br />");
                }
                else
                {
                    InnerMessage.Text = ex.GetType().ToString().Replace("\n", "<br />");
                    if (ex.StackTrace != null)
                    {
                        InnerTrace.Text = ex.StackTrace.ToString().Replace("\n", "<br />").TrimStart();
                    }
                }
            }

            log.Error("An unexpected error occured.", ex);

            Response.Clear();
            Server.ClearError();
        }
    }
}