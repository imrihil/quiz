﻿using EmakinaQuiz.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace EmakinaQuiz
{
    public class Global : HttpApplication
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Initialize the product database.
            Database.SetInitializer(new QuizDatabaseInitializer());

            log4net.Config.XmlConfigurator.Configure();
            log.Info("Application started");
        }

        /// <summary>
        /// Obsługuje na najwyższym poziomie wyjątek aplikacji.
        /// Przekierowuje do akcji kontrolera obsługującej błędy, przekazując jej wyjątek.
        /// </summary>
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();

            if (exception != null)
            {
                if (exception is HttpException)
                {
                    log.Error("Top-level HTTP Exception", exception);
                }
                else
                {
                    log.Error("Top-level Non-HTTP Exception", exception);
                }
            }
            else
            {
                log.Fatal("Top-level application error without exception object");
            }

            if (!"true".Equals(ConfigurationManager.AppSettings["ShowFullYellowScreenErrorInfo"]))
            {
                Server.Transfer("~/ErrorPage.aspx?handler=Application_Error%20-%20Global.asax", true);
            }
        }
    }
}