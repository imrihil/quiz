﻿using EmakinaQuiz.Logic;
using EmakinaQuiz.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EmakinaQuiz
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void StartGameButton_Click(object sender, EventArgs e)
        {
            string userId = User.Identity.GetUserId();
            if (userId != null)
            {
                GameActions actions = new GameActions();
                Game game = actions.GetActiveGameByUserId(userId);
                if (game == null)
                    game = actions.CreateGame(userId);
                Response.Redirect("~/Quiz/Game.aspx?gameId=" + game.GameId);
            }
            else
            {
                throw new UnauthorizedAccessException("StartGameButton_Click unauthorized access.") { Source = Request.RawUrl };
            }
        }
    }
}
