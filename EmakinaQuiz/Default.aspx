﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="EmakinaQuiz._Default" %>

<%@ Register TagPrefix="uc" TagName="GamesList" Src="~/Controls/GamesListControl.ascx" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Hello, <%: Context.User.Identity.GetUserName()  %>.</h2>

    <asp:Button ID="StartGameButton" runat="server" Text="Start game" OnClick="StartGameButton_Click" CssClass="btn btn-primary btn-lg btn-block" />

    <uc:GamesList ID="GamesListControl" runat="server"></uc:GamesList>
</asp:Content>
