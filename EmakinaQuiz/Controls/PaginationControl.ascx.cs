﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EmakinaQuiz.Controls
{
    public partial class PaginationControl : System.Web.UI.UserControl
    {

        public int PageNumber
        {
            get
            {
                int? pn = (int?)ViewState["Page"];
                return ((pn == null) ? 1 : pn.Value);
            }
            set
            {
                ViewState["Page"] = value;
            }
        }

        public int PageCount
        {
            get
            {
                int? pc = (int?)ViewState["PageCount"];
                return ((pc == null) ? 1 : pc.Value);
            }
            set
            {
                ViewState["PageCount"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitControls();
            }
        }

        private void InitControls()
        {
            PageFirst.CommandArgument = "1";
            PagePrevious.CommandArgument = (PageNumber - 1).ToString();
            PageNext.CommandArgument = (PageNumber + 1).ToString();
            PageLast.CommandArgument = PageCount.ToString();

            if (PageNumber > 1)
            {
                int start = Math.Max(PageNumber - 2, 1);
                int count = PageNumber - start;
                PageLinksBeforeRepeater.DataSource = Enumerable.Range(start, count).Select(x => x.ToString()).ToList();
                PageLinksBeforeRepeater.DataBind();
            }
            PageActive.InnerText = PageNumber.ToString();
            if (PageNumber < PageCount)
            {
                int start = PageNumber + 1;
                int count = Math.Min(PageCount + 1 - start, 2);
                PageLinksAfterRepeater.DataSource = Enumerable.Range(start, count).Select(x => x.ToString()).ToList();
                PageLinksAfterRepeater.DataBind();
            }

            if (PageNumber == 1)
            {
                PageFirstList.Attributes["class"] += " disabled";
                PagePreviousList.Attributes["class"] += " disabled";
            }
            if (PageNumber == PageCount)
            {
                PageNextList.Attributes["class"] += " disabled";
                PageLastList.Attributes["class"] += " disabled";
                PageNextSeparatorList.Visible = false;
            }
            if (PageNumber <= 3)
                PagePreviousSeparatorList.Visible = false;
            if (PageNumber >= PageCount - 2)
                PageNextSeparatorList.Visible = false;
        }

        public void Page_Click(object sender, EventArgs e)
        {
            LinkButton button = sender as LinkButton;

            PageNumber = int.Parse(button.CommandArgument);
            NameValueCollection query = HttpUtility.ParseQueryString(Request.Url.Query);
            query.Set("page", PageNumber.ToString());

            string url = Request.Url.AbsolutePath + "?" + query.ToString();
            Response.Redirect(url);
        }
    }
}