﻿using EmakinaQuiz.Logic;
using EmakinaQuiz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EmakinaQuiz.Controls
{
    public partial class GameQuestionControl : UserControl
    {
        public long GameQuestionId
        {
            get
            {
                long? gqId = (long?)ViewState["GameQuestionId"];
                return ((gqId == null) ? 0 : gqId.Value);
            }
            set
            {
                ViewState["GameQuestionId"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void QuestionAnswerButton_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            long questionAnswerId = long.Parse(button.CommandArgument);

            GameQuestionActions actions = new GameQuestionActions();
            actions.AnswerQuestion(GameQuestionId, questionAnswerId);

            Response.Redirect(Request.RawUrl);
        }

        public GameQuestion GetGameQuestion()
        {
            GameQuestionActions actions = new GameQuestionActions();
            GameQuestion gameQuestion = actions.GetGameQuestionById(GameQuestionId);
            return gameQuestion;
        }
    }
}