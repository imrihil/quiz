﻿using EmakinaQuiz.Logic;
using EmakinaQuiz.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace EmakinaQuiz.Controls
{
    public partial class GamesListControl : UserControl
    {
        #region consts

        private const string GameIdQueryString = "gameId";
        private const string PageQueryString = "page";
        private const string RowsQueryString = "rows";

        #endregion

        #region properties

        public long? CurrentGameId
        {
            get
            {
                long? gId = (long?)ViewState["GameId"];
                return ((gId == null) ? 0 : gId.Value);
            }
            set
            {
                ViewState["GameId"] = value;
            }
        }

        public int PageNumber
        {
            get
            {
                int? pn = (int?)ViewState["Page"];
                return ((pn == null) ? 1 : pn.Value);
            }
            set
            {
                ViewState["Page"] = value;
            }
        }

        public int RowsPerPage
        {
            get
            {
                int? r = (int?)ViewState["Rows"];
                return ((r == null) ? 10 : r.Value);
            }
            set
            {
                ViewState["Rows"] = value;
            }
        }

        public int PageCount
        {
            get
            {
                if (GetGames().Count == 0)
                    return 0;
                return (GetGames().Count - 1) / RowsPerPage + 1;
            }
        }

        #endregion

        #region private variables

        private string _pageTitle
        {
            get
            {
                return (HttpContext.Current.Handler as Page).Title;
            }
        }

        private ICollection<GameDetail> _games { get; set; }

        #endregion

        #region page load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HandleQueryStrings();
                InitControls();
            }
        }

        private void HandleQueryStrings()
        {
            int intValue;
            long longValue;

            if (Request.QueryString[GameIdQueryString] != null && long.TryParse(Request.QueryString[GameIdQueryString], out longValue))
            {
                CurrentGameId = longValue;
            }

            if (Request.QueryString[RowsQueryString] != null && int.TryParse(Request.QueryString[RowsQueryString], out intValue))
            {
                RowsPerPage = intValue;
            }
            else
            {
                RowsPerPage = 10;
            }

            if (Request.QueryString[PageQueryString] != null && int.TryParse(Request.QueryString[PageQueryString], out intValue))
            {
                PageNumber = intValue;
            }
            else
            {
                if (CurrentGameId != null)
                {
                    int position = GetGames().ToList().FindIndex(x => x.GameId == CurrentGameId);
                    PageNumber = position / RowsPerPage + 1;
                }
                else
                {
                    PageNumber = 1;
                }
            }
        }

        private void InitControls()
        {
            GamesGridView.DataSource = GetGames().Skip(RowsPerPage * (PageNumber - 1)).Take(RowsPerPage);
            GamesGridView.DataBind();

            if (GamesGridView.HeaderRow != null)
                GamesGridView.HeaderRow.TableSection = TableRowSection.TableHeader;

            if (PageCount > 0)
            {
                Pagination.PageNumber = PageNumber;
                Pagination.PageCount = PageCount;
            }
            else
            {
                Pagination.Visible = false;
            }
        }

        #endregion

        #region page events

        protected void GamesGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridViewRow row = e.Row;
                if (((GameDetail)e.Row.DataItem).GameId == CurrentGameId)
                {
                    row.CssClass += " table-primary";
                }
            }
        }

        #endregion

        #region helpers

        protected ICollection<GameDetail> GetGames()
        {
            if (_games == null)
            {
                int i = 0;
                GameActions actions = new GameActions();
                _games = actions.GetGames().Where(g => g.IsWon).OrderBy(g => g.Time).Select(g => new GameDetail(++i, g)).ToList();
            }
            return _games;
        }

        #endregion
    }
}
