﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GameQuestionControl.ascx.cs" Inherits="EmakinaQuiz.Controls.GameQuestionControl" %>

<asp:FormView ID="GameQuestionForm" ItemType="EmakinaQuiz.Models.GameQuestion" SelectMethod="GetGameQuestion" RenderOuterTable="false" runat="server">
    <ItemTemplate>
        <h3>Question <%#:Item.Number%></h3>
        <span><%#:Item.Question.Text%></span><br />
        <asp:Repeater ID="QuestionAnswerRepeater" ItemType="EmakinaQuiz.Models.QuestionAnswer" DataSource="<%#Item.Question.QuestionAnswer%>" runat="server">
            <ItemTemplate>
                <asp:Button ID="QuestionAnswerButton" CssClass="btn btn-primary btn-lg btn-block" OnClick="QuestionAnswerButton_Click" CommandName="QuestionAnswerId" CommandArgument="<%#:Item.QuestionAnswerId%>" Text="<%#:Item.Answer.Text%>" runat="server" />
            </ItemTemplate>
        </asp:Repeater>
    </ItemTemplate>
</asp:FormView>
