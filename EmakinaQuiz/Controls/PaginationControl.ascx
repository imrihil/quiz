﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaginationControl.ascx.cs" Inherits="EmakinaQuiz.Controls.PaginationControl" %>

<nav>
    <ul id="GamesGridViewPagination" class="pagination justify-content-end">
        <li id="PageFirstList" class="page-item" runat="server">
            <asp:LinkButton ID="PageFirst" CssClass="page-link" OnClick="Page_Click" CommandName="PageNumber" runat="server">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">First</span>
            </asp:LinkButton>
        </li>
        <li id="PagePreviousList" class="page-item" runat="server">
            <asp:LinkButton ID="PagePrevious" CssClass="page-link" OnClick="Page_Click" CommandName="PageNumber" runat="server">
                <span aria-hidden="true">&lsaquo;</span>
                <span class="sr-only">Previous</span>
            </asp:LinkButton>
        </li>
        <li id="PagePreviousSeparatorList" class="page-item disabled" runat="server">
            <span class="page-link">...</span>
        </li>
        <asp:Repeater ID="PageLinksBeforeRepeater" ItemType="System.string" runat="server">
            <ItemTemplate>
                <li id="PageListBefore" class="page-item" runat="server">
                    <asp:LinkButton ID="PageLinkBefore" CssClass="page-link" OnClick="Page_Click" CommandName="PageNumber" CommandArgument="<%#:Item%>" Text="<%#:Item%>" runat="server" />
                </li>
            </ItemTemplate>
        </asp:Repeater>
        <li id="PageListActive" class="page-item active" runat="server">
            <span id="PageActive" class="page-link" runat="server" />
        </li>
        <asp:Repeater ID="PageLinksAfterRepeater" ItemType="System.string" runat="server">
            <ItemTemplate>
                <li id="PageListAfter" class="page-item" runat="server">
                    <asp:LinkButton ID="PageLinkAfter" CssClass="page-link" OnClick="Page_Click" CommandName="PageNumber" CommandArgument="<%#:Item%>" Text="<%#:Item%>" runat="server" />
                </li>
            </ItemTemplate>
        </asp:Repeater>
        <li id="PageNextSeparatorList" class="page-item disabled" runat="server">
            <span class="page-link">...</span>
        </li>
        <li id="PageNextList" class="page-item" runat="server">
            <asp:LinkButton ID="PageNext" CssClass="page-link" OnClick="Page_Click" CommandName="PageNumber" runat="server">
                <span aria-hidden="true">&rsaquo;</span>
                <span class="sr-only">Next</span>
            </asp:LinkButton>
        </li>
        <li id="PageLastList" class="page-item" runat="server">
            <asp:LinkButton ID="PageLast" CssClass="page-link" OnClick="Page_Click" CommandName="PageNumber" runat="server">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Last</span>
            </asp:LinkButton>
        </li>
    </ul>
</nav>
