﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GamesListControl.ascx.cs" Inherits="EmakinaQuiz.Controls.GamesListControl" %>

<%@ Register TagPrefix="uc" TagName="Pagination" Src="~/Controls/PaginationControl.ascx" %>

<asp:GridView ID="GamesGridView" CssClass="table table-striped table-hover" AutoGenerateColumns="False" ShowFooter="False"
    ItemType="EmakinaQuiz.ViewModels.GameDetail" OnRowDataBound="GamesGridView_RowDataBound" GridLines="None" runat="server">
    <Columns>
        <asp:BoundField DataField="Place" SortExpression="Place" />
        <asp:BoundField DataField="UserName" HeaderText="User Name" />
        <asp:BoundField DataField="EndTime" HeaderText="Date" DataFormatString="{0:dd-MM-yyyy}" />
        <asp:BoundField DataField="TimeString" HeaderText="Time" />
    </Columns>
</asp:GridView>

<uc:Pagination ID="Pagination" runat="server"></uc:Pagination>
