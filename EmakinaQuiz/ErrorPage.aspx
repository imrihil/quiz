﻿<%@ Page Title="Error" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="EmakinaQuiz.ErrorPage" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <p></p>
    <asp:Label ID="FriendlyErrorMessage" CssClass="alert alert-danger" runat="server"></asp:Label>
    <p></p>
    <asp:Panel ID="DetailedErrorPanel" Visible="false" runat="server">
        <h4>Detailed Error:</h4>
        <p>
            <asp:Label ID="ErrorDetailedMessage" runat="server" />
        </p>
        <h4>Error Handler:</h4>
        <p>
            <asp:Label ID="ErrorHandler" runat="server" />
        </p>
        <h4>Detailed Error Message:</h4>
        <p>
            <asp:Label ID="InnerMessage" runat="server" />
        </p>
        <h4>Inner Error:</h4>
        <p>
            <asp:Label ID="InnerTrace" runat="server" />
        </p>
    </asp:Panel>
</asp:Content>
